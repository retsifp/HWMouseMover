# HWMouseMover

Uses an ATMega32U4 (Arduino Leonardo/Pro Micro) to register as a mouse at Windows and moves the mouse randomly to prevent accidential locking of the OS.