#include "HID-Project.h"

// Soll geblinkt werden kurz bevor die Maus bewegt wird? Falls nicht: Auskommentieren
#define BLINK
// Handelt es sich um einen Arduino Pro?
#define PRO

#ifdef PRO
// Pin 17 für Pro Micro
const int pinLed = 17;
#else
const int pinLed = LED_BUILTIN;
#endif

// Speicher gibt's genug, ruhig mal einen long hernehmen...
long randX;
long randY;
int signX;
int signY;

int getSign(long in) {
  // Gibt -1 zurück falls Eingabezahl durch 2 teilbar, sonst 1
  return (in%2==0)?-1:1;
}

void moveRandomly() {
  #ifdef BLINK
  // Erstmal blinkiblinki
  countdownBlink();
  #endif
  // Zufallszahlen abholen
  randX = random(2,8);
  randY = random(2,8);
  // Vorzeichen der beiden Zahlen speichern, um später dann wieder zurück zu bewegen
  signX = getSign(randX);
  signY = getSign(randY);
  // Maus zunächst um die ausgewürfelten Pixel bewegen
  Mouse.move(((int)(randX>0))*signX, ((int)(randX>0))*signY);
  // Kurz warten
  delay(2);
  // Jetzt die Maus wieder zurückbewegen
  Mouse.move(((int)(randX>0))*signX*-1, ((int)(randX>0))*signY*-1);
  // Bisher war ich mir nicht ganz sicher, ob Windows eventuell so clever ist diese plumpe Mausbewegung herauszufiltern.
  // Praktische Langzeitversuche haben gezeigt, dass diese Methode äußerst zuverlässig und störungsfrei für die PC-Benutzung ist.
}

void countdownBlink() {
  // Kurz vor dem Bewegen blinken. Eigentlich noch ein legacy-Feature aus der Erprobungsphase.
  for(int i = 0;i<19;i++) {
    // Toggeln
    digitalWrite(pinLed, !digitalRead(pinLed));
    delay(100);
  }
  // Zur Sicherheit wieder auf 0 stellen...
  #ifdef PRO
  digitalWrite(pinLed, 1);
  #else
  digitalWrite(pinLed, 0);
  #endif
}

void setup() {
  #ifdef BLINK
  pinMode(pinLed, OUTPUT);
  #endif
  Mouse.begin();
  randomSeed(analogRead(0));
}

void loop() {
  // Bewegen!
  moveRandomly();
  // Zwischen 60 und 149 s warten
  delay(1000*random(60,149));
  // Die Zeit bis zu 149 s ist wie folgt festgelegt:
  // Sehr kleine Bewegungen filtert Windows offensichtlich heraus.
  // Falls also zufällig je zwei Pixel bewegt wird, und ein typischer Sperrtimer fünf Minuten beträgt reichen 149 Sekunden aus, sodass in diesen 300 Sekunden mindestens zweimal die Maus bewegt wird.
}
